Speed Academy
=============

Source port of the singleplayer of Star Wars Jedi Knight: Jedi Academy. Focuses on staying true to the original game, possibly for speedrun purposes. Based on the original source code released by Raven, see README.txt.
