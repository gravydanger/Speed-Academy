#pragma once


void SpeedrunResetTimer();
void SpeedrunUpdateTimer();
void SpeedrunUnpauseTimer();
bool SpeedrunPauseTimer();
void SpeedrunLevelFinished();
int SpeedrunGetTotalTimeMilliseconds();
int SpeedrunGetLevelTimeMilliseconds();
